/*
=======================
Session 20
=======================
*/

// 1. Create a folder
	// WDC028-S35

// 2. Initialize a package.json file
	// WDC028-S35

		npm init

// 3. Create an index.js file
	// index.js

// 4. Install the following dependencies
	// terminal

		npm install express
		npm install mongoose
		npm install nodemon
		npm install bcrypt
		npm install cors
		npm install jsonwebtoken

// 5. Require the expressJS application
// 	index.js
		const express = require('express')
		const app = express()

		app.use(express.json())
		app.use(express.urlencoded({ extended: true }))

		app.listen(process.env.PORT || 4000, () => {
		    console.log(`API is now online on port ${ process.env.PORT || 4000 }`)
		})

// 6. Connect to the mongoDB database
// 	index.js

		// IMPORTANT NOTE: CREATE ATLAS DATABASE
		// 	database: restbooking
		// 	collection: users

		const mongoose = require('mongoose')

		mongoose.connection.once('open', () => console.log('Now connected to MongoDB Atlas.'))
		// mongoDB atlas
		mongoose.connect('mongodb+srv://admin:admin123@cluster0.7iowx.mongodb.net/restbooking?retryWrites=true&w=majority', { 
			useNewUrlParser: true, 
			useUnifiedTopology: true 
		})

		// local mongoDB

		/*mongoose.connect('mongodb://localhost:27017/restbooking', {
			useNewUrlParser: true,
			useUnifiedTopology: true,
			useFindAndModify: false
		})*/

// 7. Require the cors dependency
// 	index.js

		const cors = require('cors')

		app.use(cors())

// 8. Create a user model
// 	models > User.js

		const mongoose = require('mongoose')

		const userSchema = new mongoose.Schema({
		    firstName: {
		        type: String,
		        required: [true, 'First name is required.']
		    },
		    lastName: {
		        type: String,
		        required: [true, 'Last name is required.']
		    },
		    email: {
		        type: String,
		        required: [true, 'Email is required.']
		    },
		    password: {
		        type: String,
		        required: [true, 'Password is required']
		    },
		    isAdmin: {
		        type: Boolean,
		        default: false
		    },
		    mobileNo: {
		        type: String,
		        required: [true, 'Mobile number is required.']
		    },
		    enrollments: [
		        {
		            courseId: {
		                type: String,
		                required: [true, 'Course ID is required']
		            },
		            enrolledOn: {
		                type: Date,
		                default: new Date()
		            },
		            status: {
		                type: String,
		                default: 'Enrolled' // Alternative values are 'Cancelled' and 'Completed'.
		            }
		        }
		    ]
		})

		module.exports = mongoose.model('user', userSchema)

// 9. Import the user route and assign it to the /users endpoint.
// 	index.js

		const userRoutes = require('./routes/user')

		app.use('/api/users', userRoutes)

// 10. Import the controller and associate its methods
// 	routes > user.js

		const express = require('express')
		const router = express.Router()
		const UserController = require('../controllers/user')

		router.post('/email-exists', (req, res) => {
		    UserController.emailExists(req.body).then(result => res.send(result))
		})

		module.exports = router

// 11. Define the controller for the users and import the user model
// 	controllers > user.js

		const User = require('../models/User')
		
		module.exports.emailExists = (params) => {
			return User.find({ email: params.email }).then(result => {
				return result.length > 0 ? true : false
			})
		}

		/*
		method: post
		url: localhost:4000/api/users/email-exists
		body: 
			{
			    "email": "juantamad"
			}
		*/

// 12. Create a route to register a user
// 	routes > user.js

		router.post('/', (req, res) => {
		    UserController.register(req.body).then(result => res.send(result))
		})

// 13. Require the bcrypt package and then create a controller to register user
// 	controllers > user.js

		const bcrypt = require('bcrypt')

		module.exports.register = (params) => {
			let user = new User({
				firstName: params.firstName,
				lastName: params.lastName,
				email: params.email,
				mobileNo: params.mobileNo,
				password: bcrypt.hashSync(params.password, 10)
			})

			return user.save().then((user, err) => {
				return (err) ? false : true
			})
		}

		/*
		method: post
		url: localhost:4000/api/users
		body: 
			{
			    "firstName": "Juan",
			    "lastName": "Tamad",
			    "email": "juantamad@gmail.com".
			    "mobileNo": "09123456789",
			    "password": "juan1234"
			}
		*/

// 14. Create a route to login a user
// 	routes > user.js

		router.post('/login', (req, res) => {
		    UserController.login(req.body).then(result => res.send(result))
		})

// 15. Create an auth.js file that will contain the logic for authorization via tokens
	// auth.js

		const jwt = require('jsonwebtoken')
		const secret = 'CourseBookingAPI'

		module.exports.createAccessToken = (user) => {
			const data = {
				id: user._id,
				email: user.email,
				isAdmin: user.isAdmin
			}

			return jwt.sign(data, secret, {})
		}

// 16. Require the authentication logic and add the controller for logging in a user
// 	controllers > user.js

		module.exports.login = (params) => {
			return User.findOne({ email: params.email }).then(user => {
				if (user === null) { return false }

				const isPasswordMatched = bcrypt.compareSync(params.password, user.password)

				if (isPasswordMatched) {
					return { accessToken: auth.createAccessToken(user.toObject()) }
				} else {
					return false
				}
			})
		}

		/*
		method: post
		url: localhost:4000/api/users/login
		body:
		{
		    "email": "juantamad@gmail.com",
		    "password": "juan1234"
		}
		*/

// 17. Create a route for getting the details of the user
// 	routes > user.js
		const auth = require('../auth')

		router.get('/details', auth.verify, (req, res) => {
			const user = auth.decode(req.headers.authorization)
		    UserController.get({ userId: user.id }).then(user => res.send(user))
		})


// 18. Add the additional logic to verify and decode the user information
// 	auth.js

		module.exports.verify = (req, res, next) => {
			let token = req.headers.authorization

			if (typeof token !== 'undefined') {
				// token = token.slice(7, token.length)

				return jwt.verify(token, secret, (err, data) => {
					return (err) ? res.send({ auth: 'failed' }) : next()
				})
			} else {
				return res.send({ auth: 'failed' })
			}
		}

		module.exports.decode = (token) => {
			if (typeof token !== 'undefined') {
				// token = token.slice(7, token.length)

				return jwt.verify(token, secret, (err, data) => {
					return (err) ? null : jwt.decode(token, { complete: true }).payload
				})
			} else {
				return null
			}
		}

// 19. Create the controller logic for getting the user information
// 	controllers > user.js

		module.exports.get = (params) => {
			return User.findById(params.userId).then(user => {
				user.password = undefined
				return user
			})
		}

		/*
		method: get
		url: localhost:4000/api/users/details
		authorization: bearer token
		*/		

// 20. Create an enroll route to enroll the user to a specific course
// 	routes > user.js

		router.post('/enroll', auth.verify, (req, res) => {
			const params = {
				userId: auth.decode(req.headers.authorization).id,
				courseId: req.body.courseId
			}
		    UserController.enroll(params).then(result => res.send(result))
		})

// 21. Create a course model
// 	models > Course.js

		const mongoose = require('mongoose')

		const courseSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, 'Course name is required.']
			},
			description: {
				type: String,
				required: [true, 'Description is required.']
			},
			price: {
				type: Number,
				required: [true, 'Price is required.']
			},
			isActive: {
				type: Boolean,
				default: true
			},
			createdOn: {
				type: Date,
				default: new Date()
			},
			enrollees: [
				{
					userId: {
						type: String,
						required: [true, 'User ID is required.']
					},
					enrolledOn: {
						type: Date,
						default: new Date()
					}
				}
			]
		})

		module.exports = mongoose.model('course', courseSchema)

// 22. Add the logic for enrolling a user
// 	controllers > user.js

		const Course = require('../models/Course')

		module.exports.enroll = (params) => {
			return User.findById(params.userId).then(user => {
				user.enrollments.push({ courseId: params.courseId })

				return user.save().then((user, err) => {
					return Course.findById(params.courseId).then(course => {
						course.enrollees.push({ userId: params.userId })

						return course.save().then((course, err) => {
							return (err) ? false : true
						})
					})
				})
			})
		}

// 23. Create Miscellaneous Routes for future use
// 	routes > user.js

		router.put('/details', (req, res) => {
		    UserController.updateDetails()
		})

		router.put('/change-password', (req, res) => {
		    UserController.changePassword()
		})

		// [SECTION] Integration Routes

		router.post('/verify-google-id-token', (req, res) => {
		    UserController.verifyGoogleTokenId()
		})


// 24. Create Miscellaneous controllers for future use
// 	controllers > user.js

		module.exports.updateDetails = (params) => {
	
		}

		module.exports.changePassword = (params) => {
			
		}

		module.exports.verifyGoogleTokenId = (params) => {
			
		}

/*
=================
S20 Activity
=================
*/

// 1. Create a course route for adding a course
// 	routes > course.js

		const express = require('express')
		const router = express.Router()
		const auth = require('../auth')
		const CourseController = require('../controllers/course')

		router.post('/', auth.verify, (req, res) => {
		    CourseController.add(req.body).then(result => res.send(result))
		})

		module.exports = router

// 2. Add the main route for all the other routes
	// index.js

		const courseRoutes = require('./routes/course')

		app.use('/api/courses', courseRoutes)

// 3. Add a controller for adding a course
	// controllers > course.js

		const Course = require('../models/Course')

		module.exports.add = (params) => {
			let course = new Course({
				name: params.name,
				description: params.description,
				price: params.price
			})

			return course.save().then((course, err) => {
				return (err) ? false : true
			})
		}

		/*
		method: post
		url: localhost:4000/api/courses
		authorization: bearer token
		body: {
		    "name": "HTML",
		    "description": "A basic programming language",
		    "price": 10000
		}
		*/

// 4. Create a route for getting all the courses
	// routes > course.js

		router.get('/', (req, res) => {
		    CourseController.getAll().then(courses => res.send(courses))
		})


// 5. Add a controller for getting all the routes
	// controllers > course.js

		module.exports.getAll = () => {
			return Course.find({ isActive: true }).then(courses => courses)
		}

		/*
			method: get
			url: localhost:4000/api/courses
		*/

// 6. Create a route for getting the details of a single course
	// routes > course.js

		router.get('/:courseId', (req, res) => {
			const courseId = req.params.courseId
		    CourseController.get({ courseId }).then(course => res.send(course)) 
		})

// 7. Add a controller for getting the details of a single course
	// controllers > course.js

		module.exports.get = (params) => {
			return Course.findById(params.courseId).then(course => course)
		}

		/*
			method: get
			url: localhost:4000/api/courses/:courseId
		*/

// 8. Create a route for updating a course
	// routes > course.js

		router.put('/', auth.verify, (req, res) => {
		    CourseController.update(req.body).then(result => res.send(result))
		})


// 9. Add a controller for updating a course
	// controllers > course.js

		module.exports.update = (params) => {
			const updates = {
				name: params.name,
				description: params.description,
				price: params.price
			}

			return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
				return (err) ? false : true
			})
		}

		/*
			method: put
			url: localhost:4000/api/courses
			body: {
				"courseId": "5f4e7df6c317323420119029",
			    "name": "HTML2",
			    "description": "updated",
			    "price": 1
			}
		*/

	// index.js

		useFindAndModify: false

// 10. Create a route for soft deleting a course
	// routes > course.js

		router.delete('/:courseId', auth.verify, (req, res) => {
			const courseId = req.params.courseId
		    CourseController.archive({ courseId }).then(result => res.send(result))
		})

// 11. Add a controller for soft deleting/archiving a course
	// controllers > course.js

		module.exports.archive = (params) => {
			const updates = { isActive: false }

			return Course.findByIdAndUpdate(params.courseId, updates).then((doc, err) => {
				return (err) ? false : true
			})
		}

		/*
			method: delete
			url: localhost:4000/api/courses/courseId
		*/

/*